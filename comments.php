<?php
/**
 * The template for displaying comments
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

	<?php if ( have_comments() ) : ?>
		<h4 class="comments-title">
			<?php
				printf( _nx( 'One thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', 'fiveme' ),
					number_format_i18n( get_comments_number() ), get_the_title() );
			?>
		</h4>

		<?php fiveme_comment_nav(); ?>

		<ol class="comment-list">
			<?php
				wp_list_comments( array(
					'style'       => 'ol',
					'short_ping'  => true,
					'avatar_size' => 56,
				) );
			?>
		</ol><!-- .comment-list -->

		<?php fiveme_comment_nav(); ?>

	<?php endif; // have_comments() ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php _e( 'Comments are closed.', 'fiveme' ); ?></p>
	<?php endif; ?>


	<?php 

	$fields = array (
		
			'author' =>
    '<p class="comment-form-author"><label for="author">' . __( 'Name', 'heman' ) . '</label> ' .( $req ? '<span class="required">*</span>' : '' ) .
    '<input id="author" class="form-control" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .'" size="30"' . $aria_req . ' /></p>',
		
			'email'  =>'<p class="comment-form-email"><label for="email">' . __( 'Email', 'heman' ) . '</label> ' .( $req ? '<span class="required">*</span>' : '' ) .
    '<input id="email" class="form-control" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .'" size="30"' . $aria_req . ' /></p>',

			'url' 	 =>'<p class="comment-form-url"><label for="url">' . __( 'Website', 'heman' ) . '</label>' .'<input id="url" class="form-control" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .
    '" size="30" /></p>',
		
		
		);
		
		$args = array (
		
		'class_submit'	=>	'btn btn-primary',
		'title_reply'	=>	__( 'Write a Comment...' , 'heman'),
		'title_reply_to'=>	__( 'Reply to %s' , 'heman'),
		'fields'		=>	apply_filters( 'comment_form_default_fields' , $fields ),
		
		'comment_field' =>  '<p class="comment-form-comment"><label for="comment">' . _x( '', 'noun', 'heman' ) .
    '</label><textarea class="form-control" id="comment" style="border:1px solid grey;resize:none;height:100px;" name="comment" cols="45" rows="15" aria-required="true">' .
    '</textarea></p>',
				
		);


	 ?>




	<?php comment_form($args); ?>

</div><!-- .comments-area -->
