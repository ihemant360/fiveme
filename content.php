<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	</header><!-- .entry-header -->

<div class="row">

	<div class="col-sm-4">
		<div class="post-pic card card-outline-secondary">
		<?php
		// Post thumbnail.
		fiveme_post_thumbnail();
		?>
		</div>

		<footer class="entry-footer">
		<?php fiveme_entry_meta(); ?>
		<?php edit_post_link( __( 'Edit', 'fiveme' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
	</div>

	<div class="col-sm-8">
		<div class="entry-content">
		<?php the_excerpt(); ?>
	</div><!-- .entry-content -->

	<?php
		// Author bio.
		if ( is_single() && get_the_author_meta( 'description' ) ) :
			get_template_part( 'author-bio' );
		endif;
	?>

	
	</div>
</div>

</article><!-- #post-## -->
