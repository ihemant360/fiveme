
<header class="entry-header">
	<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
</header>

 

<div class="card">
	<div class="make-center">
		<?php $featured_image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() )); ?>
			<img class="img-fluid" src="<?php echo $featured_image; ?>" />
	</div>
</div>
 
<div class="singl-post">
<?php the_content(); ?>
</div>
