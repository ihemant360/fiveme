<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 */
?>
 
	</div><!-- .site-content -->

	<footer class="text-muted site-content navbar navbar-dark bg-inverse">
      	<div class="row">
 			<div class="col-sm-4" >
	    		<div class="make-center">
	    			<?php dynamic_sidebar( 'bottom-left' ); ?>
	    		</div>
    		</div>

    		<div class="col-sm-4" >
    			<div class="make-center">
    				<?php dynamic_sidebar( 'bottom-mid' ); ?>
    			</div>
    		</div>

    		<div class="col-sm-4" >
    			<div class="make-center">
    				<?php dynamic_sidebar( 'bottom-right' ); ?>
    			</div>
    		</div>
      	</div>
    </footer>
<?php wp_footer(); ?>

</body>
</html>
