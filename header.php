<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width , initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>


<nav class="navbar navbar-dark bg-inverse">
	<div class="row">
		<div class="col-sm-4" >
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar" aria-controls="exCollapsingNavbar" aria-expanded="false" aria-label="Toggle navigation">
    &#9776;
  </button>
		</div>
		<div class="col-sm-4" >
			<?php get_search_form(); ?>
		</div>
		<div class="col-sm-4" >
			<div class="social-icons make-center">

			</div>
		</div>
	</div>
	<div class="collapse" id="exCollapsingNavbar">
    <div class="bg-inverse p-a-1 blog-info">
      <h4><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo('title'); ?></a></h4>
      <span class="text-muted"><?php bloginfo('description'); ?></span>
    </div>

    <div class="row">

    	<div class="col-sm-4" >
    		<div class="make-center">
    			<?php dynamic_sidebar( 'top-left' ); ?>
    		</div>
    	</div>

    	<div class="col-sm-4" >
    		<div class="make-center">
    			<?php dynamic_sidebar( 'top-mid' ); ?>
    		</div>
    	</div>

    	<div class="col-sm-4" >
    		<div class="make-center">
    			<?php dynamic_sidebar( 'top-right' ); ?>
    		</div>
    	</div>
    </div> <!-- .row end  for dropdoewn menus-->

  </div> <!-- .row end for navbar -->
</nav>


<div id="content" class="site-content container">
