<form role="search" method="get"  action="<?php echo home_url( '/' ); ?>">
    <div class="form-group search-field">

    <label><!--
        <span class="screen-reader-text"><?php echo _x( 'Search Here:', 'label' ) ?></span> -->
        <input type="search" class="form-control"
            placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>"
            value="<?php echo get_search_query() ?>" name="s"
            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
    </label><!--
    <input type="submit" class="search-submit btn btn-primary"
        value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
-->
    </div>
</form>